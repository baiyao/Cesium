import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  }
  ,
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
// to\form路由对象的属性
// 1、fullpath\path\herf，全路径包含\、路径、最全路径包含#\，三种都是路径
// 2、name，路由名称，在赋值路由的时候，开发者起的名，没有就不输出
// 3、

  console.log(to,from,next(),'前置守卫');


});

export default router
