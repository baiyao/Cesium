// 封账cesium地图框架
import * as Cesium from "cesium";
export default class CesiumMap {
  private viewer: any;
  private option: any;

  constructor(option: any) {
    this.option = option;
  }

  initMap(id: string | Element) {
    // 设置初始视角
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(
      118.75688254407831, 32.035180484463005, 118.83730817872822, 32.069501957649344
    )
    // 距离
    Cesium.Camera.DEFAULT_VIEW_FACTOR = 0.01
    // ?俯仰角这个调不动
    Cesium.Camera.DEFAULT_OFFSET = new Cesium.HeadingPitchRange(180, -45, 0);



    // 初始化地球
    this.viewer = new Cesium.Viewer(id, {
      // 底图相关
      // 默认底图，baseLayerPicker为false时生效
      baseLayer: new Cesium.ImageryLayer(new Cesium.UrlTemplateImageryProvider({
        url: "http://webrd02.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",
        minimumLevel: 0,
        maximumLevel: 18,
      }), {}),
      // imageryProvider:，//旧时代的眼泪，曾经版本107之前版本设置默认底图的属性
      baseLayerPicker: false,// 是否展示图层切换小组件
      // selectedImageryProviderViewModel:{}, // 当前基础影像图层，如果设置将使用第一个可用的基础图层。仅当“ baseLayerPicker”设置为true时，此值才有效。
      // imageryProviderViewModels: [],  //可以从BaseLayerPicker中选择的ProviderViewModels数组。仅当“ baseLayerPicker”设置为true时，此值才有效。
      // selectedTerrainProviderViewModel: new Cesium.ProviderViewModel(options) // 当前基础地形图层的视图模型（如果未提供）将使用第一个可用的基础图层。仅当“ baseLayerPicker”设置为true时，此值才有效。
      // terrainProviderViewModels: [],   // 可以从BaseLayerPicker中选择的ProviderViewModels数组。仅当“ baseLayerPicker”设置为true时，此值才有效。
      // terrainProvider: // 默认加载的地形服务
      shadows: true, // 是否打开阴影
      terrainShadows: Cesium.ShadowMode.DISABLED, // 是否打开地形阴影

      // 时间轴相关
      animation: false, // 左下时间轴控制器（当前时间、倍速...）
      shouldAnimate: false, // 是否开启地图随时间轴更新
      timeline: false, // 配套的时间轴部件  
      // clockViewModel: new Cesium.ClockViewModel(clock),  // 控制当前时间的时钟实例，包含时间轴多种信息

      // 小工具
      fullscreenButton: false, // 全屏按钮部件
      // fullscreenElement: '',  // 配置全屏按钮，传入id或者dom

      homeButton: true, // home按钮
      sceneMode: Cesium.SceneMode.SCENE3D, // 默认展示的初始化场景为3D、2.5D、2D
      sceneModePicker: false, // 二三维切换部件，需要scene3DOnly非true
      scene3DOnly: false, // 当设置为true时，每个几何图形实例将仅以3D形式呈现，以节省GPU内存。
      navigationHelpButton: false, // 帮助按钮，点击展示操作指南弹窗页面
      navigationInstructionsInitiallyVisible: false, // 默认展示指南弹窗页面，无需第一次点击
      // 不常用
      geocoder: false, // 位置搜索部件（好像要加token才能正常使用）
      vrButton: false, // vr部件（可能会遇到浏览器不支持横屏锁定的报错screen.orientation.lock() is not available on this device.）
      infoBox: false, // 消息框部件（貌似是弹窗）
      selectionIndicator: false,//当用户在地球上选择一个实体时，selectionIndicator 会以可视化的方式突出显示该实体，

      // 天空盒、大气圈
      skyBox: false,  // 配置天空盒子或不显示天空盒子
      skyAtmosphere: new Cesium.SkyAtmosphere(), // 配置大气或false不显示大气,大气圈效果


      // 未整理
      useDefaultRenderLoop: true, // 控制是否继续渲染
      // targetFrameRate:24, // 控制渲染帧数
      showRenderLoopErrors: true, // 报错是否弹出错误
      useBrowserRecommendedResolution: true, // 设置为false使用window.devicePixelRatio属性
      automaticallyTrackDataSourceClocks: false, // 设置成true，使用公共clock对象，设置false，所有功能使用独立clock对象
      contextOptions: {}, // 创建场景时，配置webgl
      // mapProjection:new GeographicProjection(),   // 使用2D 或者 Columbus View modes 设置地图投影方式
      //globe: false, // 配置新的地球或隐藏地球
      orderIndependentTranslucency: true, // 如果为true且配置支持，则使用顺序无关的透明性。
      // creditViewport: '', // 提示显示容器
      // dataSources: new Cesium.DataSourceCollection(), // 小部件数据源设置

      mapMode2D: Cesium.MapMode2D.INFINITE_SCROLL, // 设置2D地图水平旋转
      projectionPicker: false, // 设置为true,  ProjectionPicker部件会被创建,    ProjectionPicker：设置地球最佳视角
      // 如果为真，渲染帧只会在需要时发生，这是由场景中的变化决定的。启用可以减少你的应用程序的CPU/GPU使用，并且在移动设备上使用更少的电池，但是需要使用Scene#requestRender在这种模式下显式地渲染一个新帧。在许多情况下，在API的其他部分更改场景后，这是必要的。请参阅使用显式呈现提高性能。
      // 不是特别明白，应该是提高渲染性能的
      requestRenderMode: true,
      // 如果requestRenderMode为true，这个值定义了在请求渲染之前允许的模拟时间的最大变化。请参阅使用显式呈现提高性能。
      maximumRenderTimeChange: 0.0,
    });

    return this.viewer;
  }


  setMapClickEvent() {
    var handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);

    handler.setInputAction((event: any) => {
      var position = event.position;

      // 将屏幕坐标转换为世界坐标
      var cartesian = this.viewer.camera.pickEllipsoid(position, this.viewer.scene.globe.ellipsoid);

      if (cartesian) {
        // 获取点击位置的地理坐标
        var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
        var longitude = Cesium.Math.toDegrees(cartographic.longitude);
        var latitude = Cesium.Math.toDegrees(cartographic.latitude);

        // 获取地图相机的四至范围
        var rectangle = this.viewer.camera.computeViewRectangle();
        var west = Cesium.Math.toDegrees(rectangle.west);
        var south = Cesium.Math.toDegrees(rectangle.south);
        var east = Cesium.Math.toDegrees(rectangle.east);
        var north = Cesium.Math.toDegrees(rectangle.north);

        // 获取地图相机的视角信息
        var camera = this.viewer.camera;
        var heading = camera.heading;
        var pitch = camera.pitch;
        var range = camera.positionCartographic.height;

        console.log("当前视角的角度 ", heading + ',' + pitch + ',' + range);

        // 输出中心点坐标和四至范围
        console.log("中心点坐标：", longitude, latitude);
        console.log("四至范围：", west + ',' + south + ',' + east + ',' + north);
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }




  /**
   * 是否关闭地图自带的底图图层（图层还在不是删除）
   * @param {boolean} val
   * 默认关闭
   */
  setBaseMapLayer(val = false) {
    // this.viewer.imageryLayers.get(0)获地图第一个底图的一些信息
    if (!this.viewer.imageryLayers.get(0)?.show) return;
    this.viewer.imageryLayers.get(0).show = val;
  }
  /**
   * 清除所有地图底图（图层已删除）
   */
  removeBaseMapLayers(layer: any = null) {
    if (layer && layer != "all") {
      this.viewer.imageryLayers.remove(layer);
    } else {
      this.viewer.imageryLayers.removeAll();
    }
  }

  /**
   * 获取地图当前所有底图
   */
  getBaseMapLayerAll() {
    return this.viewer.imageryLayers._layers;
  }

  /**
   * 添加地图底图
   * @param {*} 字段
   * 必填url和layer  {}
   */
  changeImageryLayers_tianditu(option: any) {
    // 不显示地图自带的默认底图
    this.setBaseMapLayer();
    // 添加天地图影像注记底图
    // https://webrd04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x=849&y=416&z=10
    // http://t0.tianditu.gov.cn/cva_w/wmts?tilematrix=8&layer=cva&style=default&tilerow=100&tilecol=214&tilematrixset=w&format=tiles&service=WMTS&version=1.0.0&request=GetTile&tk=77c95977eef101e67eb6ba1b52d255d3
    // const tMapImagery = new Cesium.WebMapTileServiceImageryProvider({
    //   url: `http://t0.tianditu.gov.cn/cva_w/wmts?tk=77c95977eef101e67eb6ba1b52d255d3`, //天地图
    //   layer: "cva", //WMTS请求的层名称
    //   style: "default", //WMTS请求的样式名称。
    //   tileMatrixSetID: "w", //用于WMTS请求的TileMatrixSet的标识符。
    //   format: "tiles", //从服务器检索图像的MIME类型。
    //   maximumLevel: 18, //图像提供程序支持的最大详细程度，如果没有限制，则为未定义。
    //   ...option,
    // });
    // this.viewer.imageryLayers.addImageryProvider(tMapImagery);

    let tdtLayer = new Cesium.UrlTemplateImageryProvider({
      url: "http://webrd02.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",
      minimumLevel: 3,
      maximumLevel: 18,
    });
    this.viewer.imageryLayers.addImageryProvider(tdtLayer);
  }

  loadOnlineMapLayer(mapName: any, type: any) {
    console.log(this.getBaseMapLayerAll());


    switch (mapName) {
      case "gaode":
        // 含注记
        if (type == "vec") {
          let tdtLayer = new Cesium.UrlTemplateImageryProvider({
            url: "http://webrd02.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",
            minimumLevel: 3,
            maximumLevel: 18,
          });
          this.viewer.imageryLayers.addImageryProvider(tdtLayer);
        } else if (type == "img") {
          // 影像图
          let tdtLayer = new Cesium.UrlTemplateImageryProvider({
            url: "https://webst02.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}",
            minimumLevel: 3,
            maximumLevel: 18,
          });
          this.viewer.imageryLayers.addImageryProvider(tdtLayer);
          //   路网
          let tdtLayer2 = new Cesium.UrlTemplateImageryProvider({
            url: "http://webst02.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scale=1&style=8",
            minimumLevel: 3,
            maximumLevel: 18,
          });
          this.viewer.imageryLayers.addImageryProvider(tdtLayer2);
        }

        break;

      default:
        break;
    }
  }
  flyTo(option: any) {
    let center = {
      lat: "",
      lng: "",
      height: 320544.9999996388,
      heading: 360.0,
      pitch: -90.0,
      roll: 0.0,
      ...option,
    };
    this.viewer.camera.flyTo({
      destination: Cesium.Cartesian3.fromDegrees(
        center.lng,
        center.lat,
        center.height
      ),
      // 方向，俯视和仰视的视角
      orientation: {
        heading: Cesium.Math.toRadians(center.heading), //坐标系旋转90度
        pitch: Cesium.Math.toRadians(center.pitch), //设置俯仰角度为-45度
        roll: Cesium.Math.toRadians(center.roll),
      },
    });
  }
}
